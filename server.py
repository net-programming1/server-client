import socketdef 
def send_file(client_socket):
    try:
        with open('hello.txt', 'rb') as file:
            data = file.read(1024)
            while data:
                client_socket.send(data)
                data = file.read(1024)
    except FileNotFoundError:
        print("File not found.")

def main():
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind(('0.0.0.0', 36778))
    server_socket.listen(1)
    print("Server listening on port 36778")

    while True:
        client_socket, client_addr = server_socket.accept()
        print(f"Connection from: {client_addr}")
        send_file(client_socket)
        client_socket.close()

if __name__ == '__main__':
    main()